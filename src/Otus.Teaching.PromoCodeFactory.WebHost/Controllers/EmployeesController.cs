﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.DataBaseContext;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IEfRepository<Employee> _efEmployeeRepository;

        public EmployeesController(PromocodeDbContext context)
        {          
            _efEmployeeRepository = new EFRepository<Employee>(context);
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _efEmployeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _efEmployeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Role = new RoleItemResponse()
                {
                    Name = employee.Role.Name,
                    Description = employee.Role.Description
                },
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
         /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <returns></returns>        
        [HttpPost]
        public async Task<IActionResult> CreateEmployee(Employee employee){
            
            var response = await _efEmployeeRepository.CreateAsync(employee);
            return Ok("The employee was successfully created");
        }

        /// <summary>
        /// Изменить сотрудника существуещего сотрудника
        /// </summary>
        /// <returns></returns>
        
        [HttpPut]
        public async Task<IActionResult> UpdateEmployee(Employee employee, Guid id){

            employee.Id = id;
            var response = await _efEmployeeRepository.UpdateAsync(employee);                
            return Ok($"The employee {response} successfully updated");
        }

        /// <summary>
        /// Удалить сотрудника по гуид
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteEmployee(Guid id){
            await _efEmployeeRepository.DeleteAsync(id);         
            return Ok("The employee successfully deleted");
        } 
    }
}
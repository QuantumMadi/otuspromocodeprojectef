using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.DataBaseContext;
using Microsoft.EntityFrameworkCore;
using Mapster;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Microsoft.Extensions.Configuration;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {

        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<PromocodeDbContext>
            (
                options => options.UseLazyLoadingProxies().UseNpgsql
                (
                    Configuration.GetConnectionString("DbConnection"),
                    x => x.MigrationsAssembly("Otus.Teaching.PromoCodeFactory.DataAccess")
                )
            );

            services.AddHealthChecks()
                .AddNpgSql(Configuration.GetConnectionString("DbConnection"));

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            TypeAdapterConfig<Customer, CreateOrEditCustomerRequest>
                .NewConfig()
                .Map(src => src.FirstName, dest => dest.FirstName)
                .Map(src => src.LastName, dest => dest.LastName)
                .Map(src => src.Email, dest => dest.Email);

            TypeAdapterConfig<PromoCodeShortResponse, PromoCode>
                .NewConfig()
                .Map(src => src.Id, dest => dest.Id)
                .Map(src => src.PartnerManager, dest => dest.PartnerName)
                .Map(src => src.BeginDate, dest => dest.BeginDate)
                .Map(src => src.EndDate, dest => dest.EndDate)
                .Map(src => src.ServiceInfo, dest => dest.ServiceInfo)
                .Map(src => src.Code, dest => dest.Code)
                .IgnoreNullValues(true);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health");
                endpoints.MapControllers();
            });
        }
    }
}
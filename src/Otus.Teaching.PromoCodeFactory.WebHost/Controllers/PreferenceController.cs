using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.DataAccess.DataBaseContext;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Threading.Tasks;
using System;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Класс для работы с предпочтениями клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IEfRepository<Customer> _efRepository;
        private readonly IEfRepository<Preference> _preferenceRepo;
        public PreferencesController(PromocodeDbContext context)
        {
            _efRepository = new EFRepository<Customer>(context);
            _preferenceRepo = new EFRepository<Preference>(context);
        }

        /// <summary>
        /// Лист предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Preference>>> GetAllPreferences(){
            var prefernces = await _preferenceRepo.GetAllAsync();
            return prefernces.ToList();
        }

        /// <summary>
        /// Вернуть предпочтения клиента по id
        /// </summary>
        /// <param name="guid">id клиента</param>
        /// <returns></returns>
        [HttpGet("id")]
        public async Task<ActionResult<PreferenceResponse>> GetCustomerPreferecnces(Guid guid)
        {
            var customer = await _efRepository.GetByIdAsync(guid);

            if (customer != null)
            {
                return new PreferenceResponse
                {
                    CustomerName = customer.FullName,
                    Preferences = customer.Preferences.Select(x => x.Preference).ToList()
                };
            }
            return BadRequest("The customer not found");
        }
    }
}
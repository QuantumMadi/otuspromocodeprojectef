using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.DataBaseContext
{
    public class PromocodeDbContext : DbContext
    {
        public PromocodeDbContext() {}
        public PromocodeDbContext(DbContextOptions<PromocodeDbContext> options) : base(options)
        {
            Database.Migrate();
        }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder dbContextOptionsBuilder)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            base.OnModelCreating(builder);

            builder.Entity<Employee>()
                .HasOne(x => x.Role)
                .WithMany(x => x.Employees)
                .HasForeignKey(x => x.RoleId);

            builder.Entity<Customer>().HasMany(x => x.Preferences);
            builder.Entity<Customer>().HasMany(x => x.PromoCodes);

            builder.Entity<CustomerPreference>()
                .HasKey(x => new { x.PreferenceId, x.CustomerId });

            builder.Entity<CustomerPreference>()
                .HasOne(x => x.Preference)
                .WithMany()
                .HasForeignKey(x => x.PreferenceId);

            builder.Entity<CustomerPreference>()
                .HasOne(x => x.Customer)
                .WithMany(x => x.Preferences)
                .HasForeignKey(x => x.CustomerId);
            
            builder.Entity<Role>().HasData(FakeDataFactory.Roles);
            builder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            builder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            builder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);
            builder.Entity<Customer>().HasData(FakeDataFactory.Customers);
        }
    }
}

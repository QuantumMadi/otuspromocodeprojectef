using System;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
   public interface IEfRepository<T> : 
        IRepository<T> 
        where T:BaseEntity
    {
        Task<Guid> CreateAsync(T data);
        Task DeleteAsync(Guid id);
        Task<Guid> UpdateAsync(T data);
    }
}
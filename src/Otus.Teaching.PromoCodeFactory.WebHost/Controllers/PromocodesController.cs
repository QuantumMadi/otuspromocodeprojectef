﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.DataBaseContext;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private const int _DEFAULTMONTH_ = 2;
        private readonly IEfRepository<PromoCode> _promocodeRepo;
        private readonly IEfRepository<Employee> _employessRepo;
        private readonly IEfRepository<Preference> _preferencePero;
        private readonly IEfRepository<Customer> _customerRepo;

        public PromocodesController(PromocodeDbContext context)
        {
            _promocodeRepo = new EFRepository<PromoCode>(context);
            _employessRepo = new EFRepository<Employee>(context);
            _preferencePero = new EFRepository<Preference>(context);
            _customerRepo = new EFRepository<Customer>(context);
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepo.GetAllAsync();
            return promocodes.Adapt<List<PromoCodeShortResponse>>();
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            try
            {
                var employees = await _employessRepo.GetAllAsync();
                var partnerManager = employees.FirstOrDefault(x => x.FullName == request.PartnerName);

                if (partnerManager == null)
                    throw new ArgumentNullException("Партнер с указанным именем отсутствует");

                var preferences = await _preferencePero.GetAllAsync();
                var preference = preferences.FirstOrDefault(x => x.Name == request.Preference);

                if (preference == null)
                    throw new ArgumentNullException("Указанное предпочтение отсутсвует");

                var promo = new PromoCode
                {
                    Code = request.PromoCode,
                    PartnerManager = partnerManager,
                    PartnerManagerId = partnerManager.Id,
                    PartnerName = request.PartnerName,
                    Preference = preference,
                    PreferenceId = preference.Id,
                    ServiceInfo = request.ServiceInfo,
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddMonths(_DEFAULTMONTH_),
                };

                await _promocodeRepo.CreateAsync(promo);

                var customers = await _customerRepo.GetAllAsync();
                var filteredCustomers = customers
                  .Where(x => x.Preferences
                  .Any(x => x.Preference.Name == request.Preference));

                if (!filteredCustomers.Any())
                    return Ok("Промокод успешно создан, не найдено соответствий с предпочтениями клиентов");

                foreach (var custom in filteredCustomers)
                {
                    this.UpdateCustomerPromocode(custom, promo);
                }
                return Ok("Промокод успешно добавлен");
            }
            catch (ArgumentNullException exception)
            {
                return BadRequest(exception.ParamName);
            }
        }

        private async void UpdateCustomerPromocode(Customer customer, PromoCode promocode)
        {
            customer.PromoCodes.Add(promocode);
            await _customerRepo.UpdateAsync(customer);
        }
    }
}
﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.DataAccess.DataBaseContext;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System.Linq;
using System.Collections.Generic;
using Mapster;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IEfRepository<Customer> _efCustomerRepository;
        private readonly IEfRepository<CustomerPreference> _efCustomerPreferences;
        private readonly IEfRepository<Preference> _preferences;
        private readonly IEfRepository<PromoCode> _promocodes;
        public CustomersController(PromocodeDbContext context)
        {
            _efCustomerRepository = new EFRepository<Customer>(context);
            _efCustomerPreferences = new EFRepository<CustomerPreference>(context);
            _preferences = new EFRepository<Preference>(context);
            _promocodes = new EFRepository<PromoCode>(context);
        }

        /// <summary>
        /// Вернуть список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _efCustomerRepository.GetAllAsync();

            var returnCustomers = customers.Select(x => new
            {
                x.Id,
                x.FirstName,
                x.LastName,
                x.Email
            }).ToList();

            if (!returnCustomers.Any()) return Ok("Customers not found");

            return Ok(returnCustomers);
        }

        /// <summary>
        /// Вернуть клиента по id
        /// </summary>
        /// <param name="id">id клиента</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var employee = await _efCustomerRepository.GetByIdAsync(id);
            if (employee == null)
                return NoContent();

            var promocodeShortRsp = employee
                .PromoCodes
                .Adapt<List<PromoCodeShortResponse>>();

            return Ok(new
            {
                employee.Id,
                employee.FirstName,
                employee.LastName,
                employee.Email,
                promocodeShortRsp
            });
        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = await GetCustomer(request, Guid.NewGuid());
            var id = await _efCustomerRepository.CreateAsync(customer);
            return Ok($"Customer {id} successfully created");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await GetCustomer(request, id);
            await _efCustomerRepository.UpdateAsync(customer);
            return Ok("Customer successfully updated");
        }

        /// <summary>
        /// Удалить клиента со всеми его промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _efCustomerRepository.GetByIdAsync(id);
            if (customer == null)
            {
                return BadRequest("Customer not found");
            }
            foreach (var promocode in customer.PromoCodes)
            {
                await _promocodes.DeleteAsync(promocode.Id);
            }
            await _efCustomerRepository.DeleteAsync(customer.Id);
            return Ok("Customer successfully deletred");
        }

        private async Task<Customer> GetCustomer(CreateOrEditCustomerRequest request, Guid id)
        {
            var customer = new Customer
            {
                Id = id,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Preferences = new List<CustomerPreference>(),
            };

            var prefernces = (await _preferences.GetAllAsync())
                .Where(x => request.PreferenceIds.Contains(x.Id));

            var customerPrefernces = new List<CustomerPreference>();

            foreach (var preference in prefernces)
            {
                customerPrefernces.Add(new CustomerPreference
                {
                    PreferenceId = preference.Id,
                    Preference = preference,
                    Customer = customer,
                    CustomerId = customer.Id
                });
            }
            customer.Preferences = customerPrefernces;
            return customer;
        }
    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EFRepository<T>
            : IRepository<T>, IEfRepository<T>
        where T : BaseEntity
    {
        internal DbContext _context;
        internal DbSet<T> _dbSet;
        public EFRepository(DbContext database)
        {
            _context = database;
            _dbSet = _context.Set<T>();
            if (_dbSet == null) throw new ArgumentNullException("DbSet not set");
        }
        public async Task<Guid> CreateAsync(T data)
        {
            var entity = await _dbSet.AddAsync(data);            
            await _context.SaveChangesAsync();
            return entity.Entity.Id;
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = _dbSet.Find();
            if (entity != null)
            {
                _dbSet.Remove(entity);
                await _context.SaveChangesAsync();
            }
            throw new ArgumentNullException("Entity not found");
        }

        public async Task<IEnumerable<T>> GetAllAsync() => await _dbSet.ToListAsync();

        public async Task<T> GetByIdAsync(Guid id) => await _dbSet.FirstOrDefaultAsync(x => x.Id == id);

        public virtual async Task<Guid> UpdateAsync(T data)
        {
            try
            {
                _dbSet.Attach(data);
                _context.Entry(data).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return data.Id;
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            catch (ArgumentNullException)
            {
                throw new ArgumentNullException("Entity not found");
            }

        }
    }
}